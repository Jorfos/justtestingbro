import React, { useContext } from "react";
import { PlayerContext } from "./Player";

const Test = ({ onClick, colour }) => {
  const { player } = useContext(PlayerContext);

  console.log(player);

  return (
    <div className="btn">
      <button onClick={onClick} style={{ backgroundColor: colour }}>
        {player}
      </button>
    </div>
  );
};

export default Test;

import Test from "./Test";
import { PlayerContext } from "./Player";
import { useState, useEffect, useMemo } from "react";

function App() {
  const [buttonColour, setButtonColour] = useState("green");

  const [player, setPlayer] = useState("Player: 1");

  const playersTurn = useMemo(
    () => ({ player, setPlayer }),
    [player, setPlayer]
  );

  function bigPenisEnergy() {
    if (buttonColour === "green") {
      setButtonColour("red");
      setPlayer("Player: 2");
    } else {
      setButtonColour("green");
      setPlayer("Player: 1");
    }
  }

  useEffect(() => {
    //fetch(buttonColour).then(alert(buttonColour))
    alert(buttonColour);
  }, [buttonColour]);

  return (
    <div className="App">
      <PlayerContext.Provider value={playersTurn}>
        <Test onClick={bigPenisEnergy} colour={buttonColour} />
      </PlayerContext.Provider>
    </div>
  );
}

export default App;
